angular.module('herb')
    .service('herbService', function($resource) {
        var service = this;

        var herbResource = $resource('http://localhost:8081/herbs/:herbId', {}, {
            // query: {
            //     method: 'GET',
            //     isArray: false
            // }
        });

        service.search = function(params) {
            return herbResource.query(params).$promise;
        };

        service.get = function(id) {
            return herbResource.get({
                herbId: id
            }).$promise;
        };

    });