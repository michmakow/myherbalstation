angular.module('herb-show')
    .controller('HerbShowController', function(herb, herbService, $location) {
        var vm = this;

        vm.herb = herb;

        vm.update = update;

        function update() {
            herbService.update(vm.product)
                .then(function() {
                    $location.path('/herbs');
                })
                .catch(function(response) {
                    vm.errors = response.data;
                });
        }
    });