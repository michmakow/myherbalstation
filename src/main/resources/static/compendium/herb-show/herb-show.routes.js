angular.module('herb-show')
    .config(function($routeProvider) {
        $routeProvider.when('/herbs/:herbId', {
            templateUrl: '/compendium/herb-show/herb-show.html',
            controller: 'HerbShowController',
            controllerAs: 'vm',
            resolve: {
                herb: function(herbService, $route) {
                    return herbService.get($route.current.params.herbId);
                }
            }
        });
    });