var herbListModule = angular.module('herb-list');

herbListModule.config(function($routeProvider) {
    $routeProvider.when('/herbs', {
        templateUrl: '/compendium/herb-list/herb-list.html',
        controller: 'HerbListController',
        controllerAs: 'vm'
    });
});