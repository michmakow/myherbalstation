var herbListModule = angular.module('herb-list');

herbListModule.controller('HerbListController', function(herbService, $location) {
    var vm = this;

    vm.sortOptions = [
        {
            displayName: 'Name ASC',
            value: 'name,asc'
        },
        {
            displayName: 'Name DESC',
            value: 'name,desc'
        },
        {
            displayName: 'Isolation ASC',
            value: 'isolation,asc'
        },
        {
            displayName: 'Isolation DESC',
            value: 'isolation,desc'
        },
        {
            displayName: 'Irrigation ASC',
            value: 'irrigation,asc'
        },
        {
            displayName: 'Irrigation DESC',
            value: 'irrigation,desc'
        }
    ];

    vm.params = {};

    vm.search = search;
    vm.viewHerb = viewHerb

    search();

    function viewHerb(id) {
        $location.path('/herbs/' + id);
    }

    function search() {
        herbService.search(vm.params)
            .then(function(response) {
                vm.herbs = response;
            })
            .catch(function(response) {
                alert(response.data.message);
                vm.error = response.data.message;
            });
    }
});