package com.project.myHerbalStation.repository;



import java.util.Optional;

import com.project.myHerbalStation.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, String> {

    Optional<Client> findOneByClientId(String clientId);
}
