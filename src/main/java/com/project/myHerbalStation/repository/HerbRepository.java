package com.project.myHerbalStation.repository;

import com.project.myHerbalStation.model.Herb;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HerbRepository extends JpaRepository<Herb, Long> {
//    Optional<Herb> findByName(String name);
}
