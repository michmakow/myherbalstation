package com.project.myHerbalStation.repository;

import com.project.myHerbalStation.model.BoxStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BoxStatusRepository extends JpaRepository<BoxStatus, Long> {
}
