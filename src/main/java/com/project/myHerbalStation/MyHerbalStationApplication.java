package com.project.myHerbalStation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyHerbalStationApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyHerbalStationApplication.class, args);
	}
}
