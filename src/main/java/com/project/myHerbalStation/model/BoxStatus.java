package com.project.myHerbalStation.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "boxStatuses")
@Data

public class BoxStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "box_id")
    private Box box;

    @Column(nullable = false)
    private LocalDateTime localDateTime;
    @Column(nullable = false)
    private Levels irrigation;
    @Column(nullable = false)
    private Levels isolation;

}
