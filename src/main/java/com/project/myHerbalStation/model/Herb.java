package com.project.myHerbalStation.model;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table (name = "herbs")
@Data
public class Herb {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Levels irrigation;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Levels insolation;

    @Column(length = 2000)
    private String description;

    @NotBlank
    private String image_url;

}
