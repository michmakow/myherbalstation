package com.project.myHerbalStation.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table (name = "stations")
@Data
public class Station {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private String id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public void updateStation(Station station){
        if(station.getUser() !=null){
            this.user = station.getUser();
        }
    }


}
