package com.project.myHerbalStation.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "boxes")
public class Box {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "herb_id")
    private Herb herb;

    @ManyToOne
    @JoinColumn(name = "station_id")
    private Station station;

    public void updateBox(Box box){
        if (box.getHerb() !=null){
            this.herb = box.getHerb();
        }
    }


}
