package com.project.myHerbalStation.service;

import com.project.myHerbalStation.exceptions.NotFoundException;
import com.project.myHerbalStation.model.Box;
import com.project.myHerbalStation.model.BoxStatus;
import com.project.myHerbalStation.repository.BoxStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BoxStatusService {

    @Autowired
    private BoxStatusRepository boxStatusRepository;

    public BoxStatus createBoxStatus(BoxStatus boxStatus){
        return boxStatusRepository.save(boxStatus);
    }

    public BoxStatus getBoxStatusById(Long id){
        Optional<BoxStatus> boxStatus = boxStatusRepository.findById(id);
        if (!boxStatus.isPresent()){
            throw new NotFoundException(String.format("Box Status %s does not exists.", id));
        } else {
            return boxStatus.get();
        }
    }


    public void deleteBoxStatus(Long id){
        if (!boxStatusRepository.existsById(id)){
            throw new NotFoundException(String.format("Box Status %s does not exists.", id));
        } else {
            boxStatusRepository.deleteById(id);
        }
    }
}
