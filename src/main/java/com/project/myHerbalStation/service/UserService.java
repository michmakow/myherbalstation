package com.project.myHerbalStation.service;

import com.project.myHerbalStation.exceptions.BindingResultException;
import com.project.myHerbalStation.exceptions.NotFoundException;
import com.project.myHerbalStation.model.User;
import com.project.myHerbalStation.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public User createUser(User user, BindingResult bindingResult) {
//        validateUser(user, bindingResult);
        throwIfBindingResultHasError(bindingResult);
        return userRepository.save(user);
    }

    public User getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new NotFoundException(String.format("User %s does not exists.", id));
        } else {
            return user.get();
        }
    }

//    public User updateUser(Long id, User user) {
//        Optional<User> savedUser = userRepository.findById(id);
//        if (!savedUser.isPresent()) {
//            throw new NotFoundException(String.format("User %s does not exists.", id));
//        } else {
//            User dbUser = savedUser.get();
//            dbUser.updateUser(user);
//            return userRepository.save(dbUser);
//
//
//        }
//    }

    public void deleteUser(Long id) {
        if (!userRepository.existsById(id)) {
            throw new NotFoundException(String.format("User %s does not exists.", id));
        } else {
            userRepository.deleteById(id);
        }
    }

//    private void validateUser(User user, BindingResult bindingResult) {
//        if (userRepository.existsByEmail(user.getUsername())) {
//            FieldError error = new FieldError("user",
//                    "email", String.format("Email %s already exists", user.getUsername()));
//            bindingResult.addError(error);
//        }
//    }

    private void throwIfBindingResultHasError(BindingResult bindingResult){
        if (bindingResult.hasFieldErrors()) {
            throw new BindingResultException(bindingResult);
        }
    }
}
