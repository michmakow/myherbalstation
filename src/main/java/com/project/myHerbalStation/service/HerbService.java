package com.project.myHerbalStation.service;

import com.project.myHerbalStation.exceptions.NotFoundException;
import com.project.myHerbalStation.model.Herb;
import com.project.myHerbalStation.repository.HerbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HerbService {

    @Autowired
    private HerbRepository herbRepository;

//    public Herb getHerbByName(String name){
//        Optional<Herb> herb = herbRepository.findByName(name);
//         if (!herb.isPresent()){
//             throw new NotFoundException(String.format("Herb %s does not exists." , name));
//         } else {
//             return herb.get();
//         }
//    }

    public Herb getHerbById(Long id) {
        Optional<Herb> herb = herbRepository.findById(id);
        if(!herb.isPresent()){
            throw new NotFoundException(String.format("Herb with id: %s does not exists." , id));
        } else {
            return herb.get();
        }
    }

    public List<Herb> getAllHebs() {
        return herbRepository.findAll();
    }
}
