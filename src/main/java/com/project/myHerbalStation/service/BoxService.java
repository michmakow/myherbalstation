package com.project.myHerbalStation.service;

import com.project.myHerbalStation.exceptions.NotFoundException;
import com.project.myHerbalStation.model.Box;
import com.project.myHerbalStation.repository.BoxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BoxService {

    @Autowired
    private BoxRepository boxRepository;

    public Box createBox(Box box){
        return boxRepository.save(box);
    }

    public Box getBoxById(Long id){
        Optional<Box> box = boxRepository.findById(id);
        if (!box.isPresent()){
            throw new NotFoundException(String.format("Box %s does not exists.", id));
        } else {
            return box.get();
        }
    }

    public Box updateBox(Long id, Box box){
        Optional<Box> savedBox = boxRepository.findById(id);
        if (!savedBox.isPresent()){
            throw new NotFoundException(String.format("Box %s does not exists.", id));
        } else {
            Box dbBox = savedBox.get();
            dbBox.updateBox(box);
            return boxRepository.save(dbBox);
        }
    }

    public void deleteBox(Long id){
        if (!boxRepository.existsById(id)){
            throw new NotFoundException(String.format("Box %s does not exists.", id));
        } else {
            boxRepository.deleteById(id);
        }
    }
}
