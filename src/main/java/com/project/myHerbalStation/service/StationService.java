package com.project.myHerbalStation.service;

import com.project.myHerbalStation.exceptions.NotFoundException;
import com.project.myHerbalStation.model.Station;
import com.project.myHerbalStation.model.User;
import com.project.myHerbalStation.repository.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.Optional;

@Service
public class StationService {

    @Autowired
    private StationRepository stationRepository;

    public Station createStation(Station station){
        return stationRepository.save(station);
    }

    public Station getStationById(Long id){
        Optional<Station> station = stationRepository.findById(id);
        if (!station.isPresent()){
            throw new NotFoundException(String.format("Station %s does not exist.", id));
        } else {
            return  station.get();
        }
    }

    public Station updateStation(Long id, Station station){
        Optional<Station> savedStation = stationRepository.findById(id);
        if(!savedStation.isPresent()){
            throw  new NotFoundException(String.format("Station %s not found", id));
        } else {
            Station dbStation = savedStation.get();
            dbStation.updateStation(station);
            return stationRepository.save(dbStation);
        }
    }

    public void deleteStation(Long id){
        if(!stationRepository.existsById(id)){
            throw new NotFoundException(String.format("Station %s does not exists.", id));
        } else {
            stationRepository.deleteById(id);
        }

    }
}
