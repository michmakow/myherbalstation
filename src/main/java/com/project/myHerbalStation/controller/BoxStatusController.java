package com.project.myHerbalStation.controller;

import com.project.myHerbalStation.model.BoxStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/boxStatuses")
public class BoxStatusController {
    @Autowired
    private BoxStatusController boxStatusController;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public BoxStatus createBoxStatus(@RequestBody BoxStatus boxStatus){
        return boxStatusController.createBoxStatus(boxStatus);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BoxStatus getBoxStatusById(@PathVariable("id") Long id){
        return boxStatusController.getBoxStatusById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBoxStatus(@PathVariable Long id){
        boxStatusController.deleteBoxStatus(id);
    }
}
