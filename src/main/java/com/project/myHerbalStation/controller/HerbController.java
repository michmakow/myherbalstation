package com.project.myHerbalStation.controller;

import com.project.myHerbalStation.model.Herb;
import com.project.myHerbalStation.service.HerbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/herbs")
public class HerbController {

    @Autowired
    private HerbService herbService;

//    @GetMapping("/{name}")
//    @ResponseStatus(HttpStatus.OK)
//    public Herb getHerbByName(@PathVariable("name") String name){
//        return herbService.getHerbByName(name);
//    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Herb> getAllHerbs() {
        return herbService.getAllHebs();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Herb getHerbById(@PathVariable("id") Long id){
        return herbService.getHerbById(id);
    }
}
