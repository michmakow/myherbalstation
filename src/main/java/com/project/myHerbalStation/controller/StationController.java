package com.project.myHerbalStation.controller;

import com.project.myHerbalStation.model.Station;
import com.project.myHerbalStation.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/stations")
public class StationController {
    @Autowired
    private StationService stationService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Station createStation(@RequestBody Station station ){
        return stationService.createStation(station);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Station getStationById(@PathVariable("id") Long id){
        return stationService.getStationById(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Station updateStation(
            @PathVariable Long id,
            @RequestBody Station station){
        return stationService.updateStation(id,station);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id){
        stationService.deleteStation(id);
    }
}
