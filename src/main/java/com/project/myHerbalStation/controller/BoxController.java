package com.project.myHerbalStation.controller;

import com.project.myHerbalStation.model.Box;
import com.project.myHerbalStation.service.BoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/boxes")
public class BoxController {

    @Autowired
    private BoxService boxService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Box createBox(@RequestBody Box box){
        return boxService.createBox(box);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Box getBoxById(@PathVariable("id") Long id){
        return boxService.getBoxById(id);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Box updateBox(
            @PathVariable Long id,
            @RequestBody Box box){
        return boxService.updateBox(id, box);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBox(
            @PathVariable Long id){
        boxService.deleteBox(id);
    }


}
